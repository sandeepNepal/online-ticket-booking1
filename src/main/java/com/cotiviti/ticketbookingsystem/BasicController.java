package com.cotiviti.ticketbookingsystem;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.cotiviti.ticketbookingsystem.reservation.Reservation;
import com.cotiviti.ticketbookingsystem.reservation.ReservationService;

@Controller
public class BasicController {

	// private static final Logger logger =
	// LoggerFactory.getLogger(BasicController.class);

	@Autowired
	private ReservationService reservationService;

	@GetMapping("/")
	public String home() {
		return "home";
	}

	@GetMapping("/successLogin")
	public String defaultAfterLogin(HttpServletRequest request, Authentication authentication, ModelMap map) {
		map.addAttribute("username", authentication.getName());

		for (GrantedAuthority authority : authentication.getAuthorities()) {
			if (authority.getAuthority().equals("ADMIN")) {
				return "forward:/admin";
			}
		}
		return "forward:/user";
	}
	
	@GetMapping("/user")
	public String user(Authentication authentication, ModelMap map) {
		map.addAttribute("username", authentication.getName());
		List<Reservation> reservations = reservationService.listAllByPassengerEmail(authentication.getName());
		map.put("reservations", reservations);
		return "user";
	}

	@GetMapping("/admin")
	public String admin(Authentication authentication, ModelMap map) {
		map.addAttribute("username", authentication.getName());
		List<Reservation> reservations = reservationService.listAll();
		map.put("reservations", reservations);
		return "admin";
	}

	@GetMapping("/book")
	public String book(Authentication authentication, ModelMap map) {
		map.addAttribute("username", authentication.getName());
		return "book";
	}

}
