package com.cotiviti.ticketbookingsystem.reservation;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
	public List<Reservation> findByPassengerEmail(String passengerEmail);
}
