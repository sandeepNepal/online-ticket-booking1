package com.cotiviti.ticketbookingsystem.reservation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationService {

	@Autowired
	private ReservationRepository reservationRepository;
	
	public Reservation makeReservation(Reservation reservation) {
		return reservationRepository.save(reservation);
	}
	
	public List<Reservation> listAll(){
		List<Reservation> list = new ArrayList<Reservation>();
		reservationRepository.findAll().forEach(list::add);
		return list;
	}
	
	public List<Reservation> listAllByPassengerEmail(String passengerEmail){
		List<Reservation> list = new ArrayList<Reservation>();
		reservationRepository.findByPassengerEmail(passengerEmail).forEach(list::add);
		return list;
	}
}
