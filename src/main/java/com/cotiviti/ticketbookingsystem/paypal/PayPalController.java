package com.cotiviti.ticketbookingsystem.paypal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cotiviti.ticketbookingsystem.reservation.Reservation;
import com.cotiviti.ticketbookingsystem.reservation.ReservationDTO;
import com.cotiviti.ticketbookingsystem.reservation.ReservationService;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;

@Controller
public class PayPalController {

	@Autowired
	private PayPalService service;
	
	@Autowired
	private PaymentService paymentService;
	
	@Autowired
	private ReservationService reservationService;

	public static final String SUCCESS_URL = "pay/success";
	public static final String CANCEL_URL = "pay/cancel";
	public static final String LOCALHOST = "http://localhost:8080/";
	private ReservationDTO reservationDTO;

	@PostMapping("/pay")
	public String payment(@ModelAttribute("reservationDTO") ReservationDTO reservationDTO) {
		try {
			Payment payment = service.createPayment(Double.parseDouble(reservationDTO.getTotal()), reservationDTO.getCurrency(),
					reservationDTO.getMethod(), reservationDTO.getIntent(), reservationDTO.getDescription(),
					LOCALHOST + CANCEL_URL, LOCALHOST + SUCCESS_URL);
			this.reservationDTO = reservationDTO;
			for (Links link : payment.getLinks()) {
				if (link.getRel().equals("approval_url")) {
					return "redirect:" + link.getHref();
				}
			}

		} catch (PayPalRESTException e) {

			e.printStackTrace();
		}
		return "redirect:/";
	}

	@GetMapping(value = CANCEL_URL)
	public String cancelPay() {
		return "cancel";
	}
	
	@PostMapping("/payAmount")
	public String pay(@ModelAttribute("reservationDto") ReservationDTO reservationDto, Authentication authentication) {
		com.cotiviti.ticketbookingsystem.paypal.Payment myPayment = new com.cotiviti.ticketbookingsystem.paypal.Payment();
		myPayment.setTotalPayment(Integer.parseInt(reservationDto.getTotal()));
		myPayment.setPaymentCurrency(reservationDto.getCurrency());
		myPayment.setPaymentMethod(reservationDto.getMethod());
		myPayment.setPaymentIntent(reservationDto.getIntent());
		myPayment.setPaymentDescription(reservationDto.getDescription());
		
		Reservation reservation = new Reservation();
		reservation.setPassengerName(reservationDto.getPassengerName());
		reservation.setPassengerEmail(authentication.getName());
		reservation.setPassengerPhone(reservationDto.getPassengerPhone());
		reservation.setBookingDate(reservationDto.getBookingDate());
		reservation.setToCity(reservationDto.getToCity());
		reservation.setFromCity(reservationDto.getFromCity());
		reservation.setFlightNo(reservationDto.getFlightNo());
		reservation.setFlightName(reservationDto.getFlightName());
		reservation.setAirportName(reservationDto.getAirportName());
		
		reservation.setPayment(myPayment);
		
		com.cotiviti.ticketbookingsystem.paypal.Payment payment =  paymentService.addPayment(myPayment);
		
		if(payment != null) {
			reservationService.makeReservation(reservation);
		}
		
		return "forward:/user";
	}

	@GetMapping(value = SUCCESS_URL)
	public String successPay(@RequestParam("paymentId") String paymentId, @RequestParam("PayerID") String payerId, Authentication authentication) {
		try {
			
			Payment payment = service.executePayment(paymentId, payerId);
			if (payment.getState().equals("approved")) {
				com.cotiviti.ticketbookingsystem.paypal.Payment myPayment = new com.cotiviti.ticketbookingsystem.paypal.Payment();
				myPayment.setTotalPayment(Integer.parseInt(reservationDTO.getTotal()));
				myPayment.setPaymentCurrency(reservationDTO.getCurrency());
				myPayment.setPaymentMethod(reservationDTO.getMethod());
				myPayment.setPaymentIntent(reservationDTO.getIntent());
				myPayment.setPaymentDescription(reservationDTO.getDescription());
				
				Reservation reservation = new Reservation();
				reservation.setPassengerName(reservationDTO.getPassengerName());
				reservation.setPassengerEmail(authentication.getName());
				reservation.setPassengerPhone(reservationDTO.getPassengerPhone());
				reservation.setBookingDate(reservationDTO.getBookingDate());
				reservation.setToCity(reservationDTO.getToCity());
				reservation.setFromCity(reservationDTO.getFromCity());
				reservation.setFlightNo(reservationDTO.getFlightNo());
				reservation.setFlightName(reservationDTO.getFlightName());
				reservation.setAirportName(reservationDTO.getAirportName());
				
				reservation.setPayment(myPayment);
				
				com.cotiviti.ticketbookingsystem.paypal.Payment newPayment =  paymentService.addPayment(myPayment);
				
				if(newPayment != null) {
					reservationService.makeReservation(reservation);
				}
				
				return "success";
			}
		} catch (PayPalRESTException e) {
			System.out.println(e.getMessage());
		}
		return "redirect:/";
	}

}
