package com.cotiviti.ticketbookingsystem.paypal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepsitory extends CrudRepository<Payment, Long>{

}
