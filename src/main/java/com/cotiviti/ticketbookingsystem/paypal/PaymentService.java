package com.cotiviti.ticketbookingsystem.paypal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
	@Autowired
	private PaymentRepsitory paymentRepsitory;
	
	public Payment addPayment(Payment payment) {
		return paymentRepsitory.save(payment);
	}
	
}
