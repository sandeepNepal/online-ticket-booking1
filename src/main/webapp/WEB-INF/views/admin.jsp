<!DOCTYPE html>
<html lang="en">
<head>
  <title>Admin Dashboard</title>
  <meta charset="utf-8">
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Cotiviti</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Contact</a></li>
      <li><a href="#">About Us</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> ${username}</a></li>
      <li><a href="/login"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
  </div>
</nav>

<c:if test="${empty reservations}">
<div class="container">
	  <h3>No bookings made</h3>
</div>
</c:if>

<c:if test="${not empty reservations}">
	<div class="container" style="width:1500px">
	  <h2>Your Bookings</h2>
	  <table class="table">
	    <thead>
	      <tr>
	        <th>Reservation Id</th>
	        <th>Passenger Name</th>
	        <th>Passenger Email</th>
	        <th>Passenger Phone</th>
	        <th>Booking Date </th>
	        <th>From City </th>
	        <th>To City </th>
	        <th>Flight No </th>
	        <th>Flight Name </th>
	        <th>Airport Name </th>
	        
	        <th>Payment Id</th>
	        <th>Amount</th>
	        <th>Currency</th>
	        <th>Method</th>
	        <th>Intent</th>
	        <th>Description</th>
	        
	      </tr>
	    </thead>
	    <tbody>
		      <c:forEach var="reservation" items="${reservations }" varStatus="loopCounter">
		      
		          <c:choose> 
  					<c:when test="${loopCounter.count %2 == 0}">
   						 <c:set var="color" value="#5cb85c" />
  					</c:when>
  					<c:otherwise>
    					 <c:set var="color" value="#d9534f" />
  					</c:otherwise>
				  </c:choose>
		      
		      
					<tr bgcolor="<c:out value="${color}"/>">
						<td>${reservation.reservationId }</td>
						<td>${reservation.passengerName }</td>
						<td>${reservation.passengerEmail }</td>
						<td>${reservation.passengerPhone }</td>
						<td>${reservation.bookingDate }</td>
						<td>${reservation.fromCity }</td>
						<td>${reservation.toCity }</td>
						<td>${reservation.flightNo }</td>
						<td>${reservation.flightName }</td>
						<td>${reservation.airportName }</td>
						
						<td>${reservation.payment.paymentId }</td>
						<td>${reservation.payment.totalPayment }</td>
						<td>${reservation.payment.paymentCurrency }</td>
						<td>${reservation.payment.paymentMethod }</td>
						<td>${reservation.payment.paymentIntent }</td>
						<td>${reservation.payment.paymentDescription }</td>
					</tr>
				</c:forEach>
	      
	    </tbody>
	  </table>
	</div>
</c:if>




</body>
</html>
