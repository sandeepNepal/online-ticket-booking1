<!DOCTYPE html>
<html lang="en">
<head>
  <title>Book Ticket</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  <style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: white;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  width: 640px;
  padding: 16px;
  background-color: pink;
}

/* Full-width input fields */
input[type=text], input[type=password], input[type=date] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus, input[type=date]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}
</style>
  
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">Cotiviti</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/">Home</a></li>
      <li><a href="#">Contact</a></li>
      <li><a href="#">About Us</a></li>
      <li class="active"><a href="/book">Book Ticket</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> ${username}</a></li>
      <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
  </div>
</nav>
  
<form action="pay" method="post" modelAttribute="reservationDto">
  <div class="container">
    <h3>Book Ticket</h3>
    <hr>
    <label for="psw"><b>Passenger Full Name</b></label>
    <input type="text" placeholder="Enter First Name" name="passengerName" id="passengerName" required>
    
    <label for="psw"><b>Passenger Phone No</b></label>
    <input type="text" placeholder="Enter Phone No" name="passengerPhone" id="passengerPhone" required>
    
    <label for="psw"><b>Booking Date</b></label>
    <input type="date" id="bookingDate" name="bookingDate">
    
    <label for="psw"><b>From City</b></label>
    <input type="text" placeholder="Enter City Name" name="fromCity" id="fromCity" required>
    
    <label for="psw"><b>To City</b></label>
    <input type="text" placeholder="Enter City Name" name="toCity" id="toCity" required>
    
    <label for="psw"><b>Flight Number</b></label>
    <input type="text" placeholder="Enter Flight Number" name="flightNo" id="flightNo" required>
    
    <label for="psw"><b>Flight Name</b></label>
    <input type="text" placeholder="Enter Flight Name" name="flightName" id="flightName" required>
    
    <label for="psw"><b>Airport Name</b></label>
    <input type="text" placeholder="Enter Airport Name" name="airportName" id="airportName" required>

    <hr>
    
    <div class="col-50">
        <h3>Payment</h3>
        <label for="price">Total</label>
        <input type="text" id="total" name="total" value="10" required>
        <label for="currency">Currency</label>
        <input type="text" id="currency" name="currency" placeholder="Enter Currency" required>
        <label for="method">Payment Method</label>
        <input type="text" id="method" name="method" placeholder="Payment Method" required>
        <label for="intent">Intent</label>
        <input type="text" id="intent" name="intent" value="sale" required>
        <label for="description">Payment Description</label>
        <input type="text" id="description" name="description" placeholder="Payment Description" required>

    </div>

    <button type="submit" class="registerbtn">Book</button>
  </div>
</form>

</body>
</html>
